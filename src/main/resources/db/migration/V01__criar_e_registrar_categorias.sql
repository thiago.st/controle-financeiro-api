CREATE TABLE categoria (
	id_categoria BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome_categoria VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO categoria (nome_categoria) VALUES ('Lazer');
INSERT INTO categoria (nome_categoria) VALUES ('Alimentação');
INSERT INTO categoria (nome_categoria) VALUES ('Supermercado');
INSERT INTO categoria (nome_categoria) VALUES ('Farmácia');
INSERT INTO categoria (nome_categoria) VALUES ('Outros');