package com.example.controle.financeiro.api.model;

public enum TipoLancamento {
	
	RECEITA,
	DESPESA

}
