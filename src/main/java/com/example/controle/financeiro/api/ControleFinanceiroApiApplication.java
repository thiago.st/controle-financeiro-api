package com.example.controle.financeiro.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.example.controle.financeiro.api.config.property.ControleFinanceiroApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(ControleFinanceiroApiProperty.class)
public class ControleFinanceiroApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControleFinanceiroApiApplication.class, args);
	}
}
