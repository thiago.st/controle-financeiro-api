package com.example.controle.financeiro.api.service;

import com.example.controle.financeiro.api.model.Pessoa;
import com.example.controle.financeiro.api.repository.PessoaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    public Pessoa atualizar(Long id_pessoa, Pessoa pessoa) {
        Pessoa pessoaSalva = buscarPessoaPeloCodigo(id_pessoa);

        BeanUtils.copyProperties(pessoa, pessoaSalva, "id_pessoa");
        return pessoaRepository.save(pessoaSalva);
    }

    public void atualizarPropriedadeAtivo(Long id_pessoa, Boolean ativo) {
    	Pessoa pessoaSalva = buscarPessoaPeloCodigo(id_pessoa);
    	pessoaSalva.setAtivo(ativo);
    	pessoaRepository.save(pessoaSalva);
    }
    
    public Pessoa buscarPessoaPeloCodigo(Long id_pessoa) {
    	Pessoa pessoaSalva = pessoaRepository.findOne(id_pessoa);
    	if(pessoaSalva == null) {
    		throw new EmptyResultDataAccessException(1);
    	}
    	return pessoaSalva;
    }
}
