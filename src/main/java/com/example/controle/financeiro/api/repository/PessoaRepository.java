package com.example.controle.financeiro.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.controle.financeiro.api.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
