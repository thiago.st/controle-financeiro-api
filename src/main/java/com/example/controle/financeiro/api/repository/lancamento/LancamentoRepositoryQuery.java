package com.example.controle.financeiro.api.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.controle.financeiro.api.model.Lancamento;
import com.example.controle.financeiro.api.repository.filter.LancamentoFilter;
import com.example.controle.financeiro.api.repository.projections.ResumoLancamento;

public interface LancamentoRepositoryQuery {
	
	public Page<Lancamento> filtrar(LancamentoFilter lancamentoFilter, Pageable pageable);
	public Page<ResumoLancamento> resumir(LancamentoFilter lancamentoFilter, Pageable pageable);

}
