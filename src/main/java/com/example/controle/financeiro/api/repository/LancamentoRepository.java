package com.example.controle.financeiro.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.controle.financeiro.api.model.Lancamento;
import com.example.controle.financeiro.api.repository.lancamento.LancamentoRepositoryQuery;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery{

}
